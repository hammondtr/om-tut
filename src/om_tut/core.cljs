(ns ^:figwheel-always om-tut.core
  (:require-macros [cljs.core.async.macros :refer [go]])
  (:require [om.core :as om :include-macros true]
            [om.dom :as dom :include-macros true]
            [cljs.core.async :refer [put! chan <!]]))

(enable-console-print!)

(defonce app-state
  (atom
    {:status "don't know how to update state yet lol"
     :contacts
     [{:first "Ben" :last "Bitdiddle" :email "benb@mit.edu"}
      {:first "Alyssa" :middle-initial "P" :last "Hacker" :email "aphacker@mit.edu"}
      {:first "Eva" :middle "Lu" :last "Ator" :email "eval@mit.edu"}
      {:first "Louis" :last "Reasoner" :email "prolog@mit.edu"}
      {:first "Cy" :middle-initial "D" :last "Effect" :email "bugs@mit.edu"}
      {:first "Lem" :middle-initial "E" :last "Tweakit" :email "morebugs@mit.edu"}]}))


(defn middle-name [{:keys [middle middle-initial]}]
    (cond
        middle (str " " middle)
        middle-initial (str " " middle-initial ".")))

(defn display-name [{:keys [first last] :as contact}]
    (str (:last contact) ", " (:first contact) (middle-name contact)))

(defn contact-view [contact owner]
    (reify
        om/IRender
        (render [this]
            (dom/li
                nil
                (dom/span nil (display-name contact))
                (dom/button nil "delete")))))

;; base component -- root takes raw "data"
(defn contacts-view [data owner]
    (reify
        om/IRender
        (render [this]
            (dom/div nil
                (dom/h2
                    #js {:className "test"}
                    "Contact List")
                (apply dom/ul nil
                    (om/build-all contact-view (:contacts data)))))))

;; om root!
(om/root
    contacts-view
    app-state
    {:target (. js/document (getElementById "app"))})


(defn output-box [data owner]
    (reify
        om/IRender
        (render [this]
            (dom/div nil
                (:status data)))))

;; an output box on the screen lol
(om/root output-box app-state {:target (. js/document (getElementById "outbox"))})


(defn on-js-reload []
  ;; optionally touch your app-state to force rerendering depending on
  ;; your application
  ;; (swap! app-state update-in [:__figwheel_counter] inc)
)
